# Overview

You have been invited to a project that requires Git skills.  To demonstrate your skills, fork this repository and issue a pull request with your name at the bottom of committers.md

Two things to keep in mind:

* We use the [git-flow branching model](http://nvie.com/posts/a-successful-git-branching-model/).
* Comments matter.

If you have privacy concerns about using your real name, use a temporary BitBucket account and a fake name.  